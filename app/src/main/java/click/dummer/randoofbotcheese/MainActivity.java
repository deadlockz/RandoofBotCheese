package click.dummer.randoofbotcheese;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {
    private EditText editText;
    private TextView textView;
    private SharedPreferences mPreferences;
    private String dialog;
    private Random ra;

    private static final String[] HINTS = {
            "Was?!",
            "Was war noch gleich?",
            "Sag mir dein Thema...",
            "Kann ich helfen?"
    };

    private static final String[] ANSWERS = {
            "Ha, ha, ha! %s würde ich mir nochmal überlegen!",
            "Bei %s sagen meine Innereien heute klar: ja.",
            "Ich finde, alle Menschen sollten sich zu %s ganz klar bekennen.",
            "Kunfuzius sagt, dass %s deinem Herrn nicht immer zu Glanz verhilft.",
            "Sie nennen mir falsche Daten. Ich ignoriere %s. %s kann nur Verwirrung stiften.",
            "Ob nun Sonne, Mond oder %s - wer kann da schon was zu sagen.",
            "ja. %s kann, muss aber nicht.",
            "Du kannst %s machen, aber dann isset halt Scheiße.",
            "Oh, %s is sicher.",
            "Über %s wurde immer schon so entschieden, dass man es tun sollte.",
            "Mache %s auf jeden Fall nicht alleine, ok?",
            "Die Aussichten für Erfolg stehen bei %s ganz gut.",
            "%s ? Frage mich später nochmal.",
            "Wenn du %s machst, möchte ich dir lieber jetzt noch keine Antwort geben.",
            "Also wenn ein %s Nachts von Rechts nach Katze läuft, dann kannst du davon ausgehen, dass was nicht stimmt.",
            "Auf %s solltest du besser nicht zählen.",
            "Teile von %s könnten dich verunsichern.",
            "Hinterher wird %s ein Bestandteil deines Bewusstseins.",
            "Das %s kann ein Geschenk sein. Versuche seine Schönheit zu erleben.",
            "Wenn %s zu dir gehört, dann wird es auch zu dir kommen.",
            "Mache %s zu einem Geschenk für andere.",
            "Die Erfahrung mit %s ist das einzige, was Du wirklich besitzt.",
            "Lasst uns Lichter anzünden, anstatt über %s zu klagen.",
            "Viele Orakel sagen, %s sei die Essenz des Erfolgs.",
            "Bei %s sollte man erstmal abwarten und Tee trinken.",
            "Hätte, hätte, %sette.",
            "Niemand hat vor %s zu bauen. Wir wissen alle, was am Ende dabei schon mal raus kam, oder?",
            "Ich muss mich nochmal genauer konzentrieren, um bei %s dir einen Rat geben zu können."
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        try {
            ActionBar ab = getSupportActionBar();
            if (ab != null) {
                ab.setDisplayShowHomeEnabled(true);
                ab.setHomeButtonEnabled(true);
                ab.setDisplayUseLogoEnabled(true);
                ab.setLogo(R.mipmap.ic_launcher);
                ab.setTitle(" " + getString(R.string.app_name));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        editText = (EditText) findViewById(R.id.editText);
        textView = (TextView) findViewById(R.id.textView);

        mPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        if (!mPreferences.contains("dialog")) {
            mPreferences.edit().putString("dialog", "...").commit();
        }
        ra = new Random();

        dialog = mPreferences.getString("dialog", "...");
        textView.setText(dialog);
        editText.setHint(HINTS[ra.nextInt(HINTS.length)]);

        editText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_SEND ||
                        keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER &&
                                keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    String in = editText.getText().toString().trim();
                    String fo = ANSWERS[ra.nextInt(ANSWERS.length)];
                    dialog = String.format(fo, in) + "\n\n" + dialog;
                    int leng = dialog.length();
                    if (leng > 1000) leng = 1000;
                    dialog = dialog.substring(0, leng);
                    textView.setText(dialog + "...");
                    editText.setText("");
                    mPreferences.edit().putString("dialog", dialog).apply();
                    view.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            editText.requestFocus();
                        }
                    }, 250);
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        dialog = mPreferences.getString("dialog", "...");
        textView.setText(dialog);
        editText.setHint(HINTS[ra.nextInt(HINTS.length)]);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mPreferences.edit().putString("dialog", dialog).apply();
    }
}
